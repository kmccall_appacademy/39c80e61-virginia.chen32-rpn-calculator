class RPNCalculator
  def initialize
    @stack = []
  end

  def push(n)
    @stack << n
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map do |el|
      if operation?(el)
        el.to_sym
      else Integer(el)
      end
    end
  end

  def evaluate(str)
    tokens = tokens(str)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    self.value
  end

  private

  def perform_operation(symbol)
    raise "calculator is empty" if @stack.length<2
    op_one = @stack.pop
    op_two = @stack.pop
    case symbol
    when :+
      result = op_one + op_two
      @stack << result
    when :-
      result = (-1)*(op_one - op_two)
      @stack << result
    when :*
      result = op_one.to_f*op_two.to_f
      @stack << result
    when :/
      result = 1/(op_one.to_f/op_two.to_f)
      @stack << result
    else
      @stack << op_one
      @stack << op_two
      raise "No such operation #{symbol}"
    end
  end

  def operation?(char)
    [:+,:-,:*,:/].include?(char.to_sym)
  end

end
